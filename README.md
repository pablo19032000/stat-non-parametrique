
# Projet de Statistiques Non Paramétriques

### Auteurs : 
Paul Mboucheko Mboucheko, Kouame Konan  
**Date** : 8 avril 2022

## Résumé
Ce projet explore plusieurs méthodes d'estimation statistique non paramétriques, 
en se concentrant sur l’estimation de densité et les techniques de régression. 
L'objectif principal est d'estimer une distribution inconnue et de construire des 
modèles non paramétriques pour analyser des données complexes. L’analyse inclut l'estimateur 
de densité de Parzen-Rosenblatt et le modèle de régression de Nadaraya-Watson, 
avec un accent particulier sur la sélection optimale des paramètres via la 
validation croisée.

## Structure du Projet
Le projet est divisé en trois parties principales :

1. **Partie A : Estimation de Densité**
    - **Distribution Connue** : En utilisant l'estimateur de Parzen-Rosenblatt, 
        nous analysons un jeu de données avec une distribution \( f \) connue. 
        L'objectif est de trouver le paramètre de lissage optimal \( h \) en
         utilisant différents critères :
      - **Critère \( M_0(h) \)** : Minimise l'erreur quadratique intégrée.
      - **Critère \( M_1(h) \)** : Minimise l'erreur quadratique moyenne.
    - **Distribution Inconnue** : Lorsque la vraie distribution est inconnue, 
        nous utilisons la validation croisée pour approximer le paramètre de 
        lissage optimal.

2. **Partie B : Optimisation de l’Estimateur Non Paramétrique**
    - **Objectif** : Déterminer le paramètre de lissage optimal \( h \) qui 
        minimise l'erreur quadratique moyenne (MSE) pour l'estimation de densité, 
        en utilisant la validation croisée.
    - **Méthodes et Résultats** : Nous appliquons le noyau gaussien et obtenons un 
        paramètre de lissage optimal qui est ensuite visualisé.

3. **Partie C : Régression Non Paramétrique avec le Modèle de Nadaraya-Watson**
    - **Définition du Modèle** : Nous estimons la fonction de régression \( m(x) \) en 
        utilisant la méthode de Nadaraya-Watson, avec le noyau gaussien pour le 
        lissage.
    - **Sélection du Paramètre Optimal** : La validation croisée est utilisée pour 
        déterminer le meilleur paramètre de lissage pour le modèle de régression, minimisant l'erreur quadratique moyenne.
    - **Visualisation** : Les résultats sont visualisés pour comparer la fonction 
    de régression estimée \( m_{nh}(x) \) avec les données réelles.

## Principaux Résultats
- **Estimation de Densité** : Les densités estimées approchent étroitement les 
    distributions réelles lorsqu’on utilise les paramètres de lissage optimaux.
- **Estimation de Régression** : Le modèle de régression de Nadaraya-Watson 
    parvient à approximer efficacement la fonction cible, montrant un lissage 
    efficace lorsque le paramètre optimal est choisi.

## Fichiers
- `Rapport.Rmd` : Contient le code pour l'analyse complète, incluant l'estimation de densité, 
    de régression, la sélection de paramètres et la visualisation.
- `dens.txt` : Contient les jeux de données utilisés pour l'estimateur de densité.
- `reg.txt` : Contient les jeux de données utilisés pour la regression non paramétrique.
- Les  images ilustratives sont dans le fichier pdf nommé `Rapport_de_projet.pdf`

## Méthodes
### 1. Estimateur de Densité de Parzen-Rosenblatt
L'estimateur de Parzen-Rosenblatt calcule des estimations de densité en sélectionnant 
un paramètre de lissage \( h \) optimal basé sur la validation croisée. Cette méthode est 
appliquée sur des jeux de données avec des distributions connues et inconnues.

### 2. Estimateur de Régression de Nadaraya-Watson
La méthode de Nadaraya-Watson permet d’estimer l’espérance conditionnelle \( E[Y|X=x] \) 
en utilisant un noyau gaussien pour lisser les observations. Les paramètres optimaux sont 
sélectionnés par validation croisée non biaisée (UCV).

## Visualisations
- **Graphiques de Densité** : Affichent les fonctions de densité réelle vs estimée pour 
    différentes valeurs de \( h \).
- **Graphiques d’Erreur** : Visualisent l'erreur de validation croisée pour un
     éventail de valeurs de lissage.
- **Graphiques de Régression** : Illustrent la fonction de régression estimée \( m_{nh}(X_i) \) 
    en comparaison avec les observations réelles.

## Exécution
1. **Prérequis** : Installer R et les bibliothèques nécessaires, incluant `ggplot2` 
    pour les visualisations.
2. **Exécuter l’Analyse** : Lancer le script `main.R` pour effectuer l'estimation 
    de densité, de régression et générer les visualisations.
3. **Consulter les Résultats** : Les graphiques générés seront enregistrés dans 
    le dossier `plots/`, permettant d’observer les comparaisons entre fonctions réelles et estimées.

## Conclusion
    Ce projet démontre l'efficacité des méthodes non paramétriques pour estimer des 
    distributions et des fonctions de régression inconnues, soulignant l'importance de la 
    sélection optimale des paramètres pour obtenir des approximations précises.
